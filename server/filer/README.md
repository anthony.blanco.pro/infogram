# Filer : Fake File Server

This filer is use as storage for media content like : photos, gif, videos, json, csv, ...

## Download

To download the files from the server, you must use the following url :

```json
localhost:3000/filer/download
```

The body of the request must be in the following form :

```json
{
 "file": "badge/ad.png"
}
```

## Upload

To upload the files from the server, you must use the following url :

```json
localhost:3000/filer/upload
```

The request must be in the following form :

- form-data
- must contain these entries :

  File : `the file to be saved`

  Path : `userId / group / badge / default`

  Context : `profile / background / postId / groupName / pictureName`

examples :

```js
file: 'ad.png'
path: 'badge'
context: 'ad'
```

```js
file: 'picture.png'
path: 'b4d747bf-8920-47b4-a9b9-b62b2f7f0eb2'
context: 'profile'
```

```js
file: 'picture.png'
path: 'group'
context: 'TestGroup_0'
```

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
const { THREE_DAYS } = require('../utils/variables')

dotenv.config()

module.exports.generateAccessToken = async userId => {
	return await jwt.sign({ userId: userId }, process.env.TOKEN_SECRET, {
		expiresIn: THREE_DAYS,
	})
}

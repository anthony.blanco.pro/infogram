/**
 * Transform a promise array to sort array
 *
 * @param {Promise[]} promiseArray
 * @returns any[]
 */
module.exports.promiseToSortArray = async promiseArray => {
	let array = await Promise.all(promiseArray).then(results => {
		return results
	})
	array = array.sort((a, b) => b.count - a.count)
	return array
}

const { PrismaClient } = require('.prisma/client')

const prisma = new PrismaClient()

class FavoriteService {
	/**
	 * Get page of favorites
	 *
	 * @param {string} userId
	 * @returns Post[]
	 */
	static async getFavorite(userId, page, limit) {
		return await prisma.favorite.findMany({
			select: {
				Post: {
					include: {
						Tags: true,
					},
				},
			},
			where: {
				userId: userId,
			},
			orderBy: {
				favoriteAt: 'desc',
			},
			skip: page * limit,
			take: limit,
		})
	}

	/**
	 * Get favorite by ids
	 *
	 * @param {string} userId
	 * @param {string} postId
	 * @returns Favorite
	 */
	static async getById(userId, postId) {
		return await prisma.favorite.findUnique({
			where: {
				userId_postId: {
					postId: postId,
					userId: userId,
				},
			},
		})
	}

	/**
	 * Create new favorite
	 *
	 * @param {string} userId
	 * @param {string} postId
	 * @returns Favorite
	 */
	static async create(userId, postId) {
		return await prisma.favorite.create({
			data: {
				userId: userId,
				postId: postId,
			},
		})
	}

	/**
	 * Delete favorite
	 *
	 * @param {string} userId
	 * @param {string} postId
	 * @returns Favorite
	 */
	static async delete(userId, postId) {
		return await prisma.favorite.delete({
			where: {
				userId_postId: {
					userId: userId,
					postId: postId,
				},
			},
		})
	}
}

module.exports = FavoriteService

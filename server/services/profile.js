const { PrismaClient } = require('@prisma/client')

const prisma = new PrismaClient()

class ProfileService {
	/**
	 * Get an user's profile by userId
	 *
	 * @param {string} userId
	 * @returns Profile
	 */
	static async getProfile(userId) {
		return await prisma.profile.findUnique({
			where: {
				userId: userId,
			},
		})
	}

	/**
	 * Create an user's profile
	 *
	 * @param {string} userId
	 * @param {string} description
	 * @param {string} image
	 * @param {string} backgroundImage
	 * @returns Profile
	 */
	static async create(
		userId,
		description = '',
		image = 'default/profile.png',
		backgroundImage = 'default/background.png'
	) {
		return await prisma.profile.create({
			data: {
				User: {
					connect: {
						id: userId,
					},
				},
				description: description,
				image: image,
				backgroundImage: backgroundImage,
			},
		})
	}

	/**
	 * Update an user's profile
	 *
	 * @param {string} userId
	 * @param {string} description
	 * @param {string} image
	 * @param {string} backgroundImage
	 * @param {string} visibility
	 * @returns Profile
	 */
	static async update(userId, description, image, backgroundImage, visibility) {
		const profile = await this.getProfile(userId)
		return profile
			? await prisma.profile.update({
					where: {
						userId: userId,
					},
					data: {
						description: description,
						image: image,
						backgroundImage: backgroundImage,
						visibility: visibility,
					},
			  })
			: profile
	}

	/**
	 * Delete an user's profile
	 *
	 * @param {string} userId
	 * @returns Profile
	 */
	static async delete(userId) {
		const profile = await this.getProfile(userId)
		return profile
			? await prisma.profile.delete({
					where: {
						userId: userId,
					},
			  })
			: profile
	}
}

module.exports = ProfileService

const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

class MessageService {
	/**
	 * Get a page of messages
	 *
	 * @param {string} userId
	 * @param {string} receiverId
	 * @param {number} nbPage
	 * @param {number} nbElem
	 * @returns Message[]
	 */
	static async getPage(userId, receiverId, nbPage, nbElem) {
		return await prisma.message.findMany({
			where: {
				OR: [
					{
						emitterId: userId,
						receiverId: receiverId,
					},
					{
						emitterId: receiverId,
						receiverId: userId,
					},
				],
			},
			skip: nbPage * nbElem,
			take: nbElem,
			orderBy: {
				sendAt: 'desc',
			},
		})
	}

	/**
	 * Get the nonread messages for a user
	 *
	 * @param {string} userId
	 * @returns
	 */
	static async getNew(userId) {
		return await prisma.message.groupBy({
			by: ['emitterId'],
			count: true,
			where: {
				receiverId: userId,
				openAt: null,
			},
		})
	}

	/**
	 * Get message by his id
	 *
	 * @param {string} messageId
	 * @returns
	 */
	static async getMessage(messageId) {
		return await prisma.message.findUnique({
			where: {
				messageId: messageId,
			},
		})
	}

	/**
	 * Get page of emitters for a user
	 *
	 * @param {string} userId
	 * @returns User[]
	 */
	static async getEmitters(userId, page, limit) {
		return await prisma.message.findMany({
			select: {
				Emitter: {
					select: {
						id: true,
						nickname: true,
					},
				},
			},
			distinct: ['emitterId'],
			where: {
				Receiver: {
					id: userId,
				},
			},
			orderBy: {
				sendAt: 'desc',
			},
			take: limit,
			skip: page * limit,
		})
	}

	/**
	 * Create a new message
	 *
	 * @param {string} emitterId
	 * @param {string} receiverId
	 * @param {JSON} content
	 * @returns Message
	 */
	static async create(emitterId, receiverId, content = null) {
		return await prisma.message.create({
			data: {
				content: content,
				Emitter: {
					connect: {
						id: emitterId,
					},
				},
				Receiver: {
					connect: {
						id: receiverId,
					},
				},
			},
		})
	}

	/**
	 * Set messages open
	 *
	 * @param {string} userId
	 * @param {string[]} messages
	 * @returns Message
	 */
	static async open(userId, messages) {
		return await prisma.message.updateMany({
			where: {
				receiverId: userId,
				messageId: {
					in: messages,
				},
			},
			data: {
				openAt: new Date().toISOString(),
			},
		})
	}

	/**
	 * Delete a message
	 *
	 * @param {string} messageId
	 * @returns
	 */
	static async delete(messageId) {
		return await prisma.message.update({
			where: {
				messageId: messageId,
			},
			data: {
				content: null,
			},
		})
	}
}

module.exports = MessageService

const express = require('express')
const MessageController = require('../controllers/message')
const {
	authenticateToken,
	authenticateMessage,
} = require('../middlewares/auth')

const messageRoutes = express.Router()

messageRoutes.get('/new/:userId', authenticateToken, MessageController.getNew)
messageRoutes.get(
	'/emitters/:userId',
	authenticateToken,
	MessageController.getEmitters
)
messageRoutes.get(
	'/:userId/:receiverId',
	authenticateToken,
	MessageController.getMessage
)
messageRoutes.post('/:userId', authenticateToken, MessageController.create)
messageRoutes.put('/:userId', authenticateToken, MessageController.open)
messageRoutes.delete(
	'/:messageId',
	authenticateToken,
	authenticateMessage,
	MessageController.delete
)

module.exports = messageRoutes

const CredentialService = require('../services/credential')
const { handleError } = require('../utils/error')
const { generateAccessToken } = require('../utils/token')
const { THREE_DAYS } = require('../utils/variables')

class CredentialController {
	/**
	 * Return true if login and password his correct
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async login(req, res) {
		try {
			const { login, password } = req.body
			const { user } = await CredentialService.login(login, password)
			if (user) {
				const token = await generateAccessToken(user.id)
				res
					.cookie('jwt', token, { expire: THREE_DAYS + Date.now() })
					.status(200)
					.json(user)
				return
			}
			res.status(403).send()
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Change user password
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async changePassword(req, res) {
		try {
			const { userId } = req.params
			const { password } = req.body
			await CredentialService.changePassword(userId, password)
			res.status(201).send()
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get user password errors
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getErrors(req, res) {
		try {
			const { userId } = req.params
			const credential = await CredentialService.getById(userId)
			res.json(credential?.error)
		} catch (error) {
			handleError(res, error)
		}
	}
}

module.exports = CredentialController

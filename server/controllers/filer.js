const fs = require('fs')
const GroupService = require('../services/group')
const PostService = require('../services/post')
const { EXTENSIONS } = require('../utils/variables')
const { handleError } = require('../utils/error')
const ProfileService = require('../services/profile')

class FilerController {
	/**
	 * Upload file to the server
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async upload(req, res) {
		try {
			if (!req.files) {
				res.status(400).send()
			} else {
				// Use the name of the input field (i.e. "file") to retrieve the uploaded file
				const file = req.files.file
				const { path, context } = req.body

				const user = res.locals.user
				const extension = file.name.split('.')[1]
				let filename = `${context}.${extension}`

				let filepath = `${path}`
				let post = false
				const groupFlag = path === 'group'
				let group

				// Add the post dir when the file is in a post or group dir
				if (groupFlag) {
					group = await GroupService.getById(context)
					if (group.ownerId !== user.id) {
						res.status(403).send()
						return
					}
					filepath += `/${context}`
					filename = `background.${extension}`
				} else if (context !== 'profile' && context !== 'background') {
					filepath += '/post'
					post = true
				} else {
					if (context === 'background')
						await ProfileService.update(
							path,
							undefined,
							undefined,
							filepath + '/' + filename,
							undefined
						)
					if (context === 'profile')
						await ProfileService.update(
							path,
							undefined,
							filepath + '/' + filename,
							undefined,
							undefined
						)
				}

				let dir = './server/filer/' + filepath

				if (!fs.existsSync(dir)) {
					fs.mkdirSync(dir, { recursive: true })
				}

				// Get meta data
				const data = {
					name: file.name,
					extension: extension,
					mimetype: file.mimetype,
					size: file.size,
				}

				// Use the mv() method to place the file in upload directory
				file.mv(dir + '/' + filename)

				// If context is a post, add meta data to it
				if (post) {
					await PostService.updateContent(context, data)
				}
				if (groupFlag && group) {
					await GroupService.update(
						group.name,
						group.ownerId,
						group.description,
						filepath + '/' + filename
					)
				}
				//send response
				res.json({
					success: true,
					message: 'File is uploaded',
					data: data,
				})
			}
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Download server file
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async download(req, res) {
		try {
			const { file } = req.params

			// Path for files
			const root = './server/filer/'
			const extension = file.split('.')[1]

			// Prevent ../ and correct file
			if (EXTENSIONS.find(ex => ex === extension) === undefined) {
				res.status(403).send()
				return
			}
			if (fs.existsSync(root + file)) {
				res.sendFile(file, {
					root: root,
				})
				return
			} else {
				res.send()
			}
		} catch (error) {
			handleError(res, error)
		}
	}
}

module.exports = FilerController

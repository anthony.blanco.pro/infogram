const MessageService = require('../services/message')
const { handleError } = require('../utils/error')

class MessageController {
	/**
	 * Get a page of messages
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getMessage(req, res) {
		try {
			const { userId, receiverId } = req.params
			const { page, limit } = req.query

			const nbPage = page ? Number.parseInt(page) : 0
			const nbLimit = limit ? Number.parseInt(limit) : 25
			const messages = await MessageService.getPage(
				userId,
				receiverId,
				nbPage,
				nbLimit
			)
			res.json(messages)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get the nonread messages for a user
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getNew(req, res) {
		try {
			const { userId } = req.params

			const messages = await MessageService.getNew(userId)
			res.json(messages)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get all emitters for a user
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getEmitters(req, res) {
		try {
			const { userId } = req.params
			const { page, limit } = req.query

			const nbPage = page ? Number.parseInt(page) : 0
			const nbLimit = limit ? Number.parseInt(limit) : 25
			const users = await MessageService.getEmitters(userId, nbPage, nbLimit)
			res.json(users)
		} catch (error) {
			handleError(res, error)
		}
	}
	/**
	 * Create a new message
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async create(req, res) {
		try {
			const { userId } = req.params
			const { receiverId, content } = req.body
			const message = await MessageService.create(userId, receiverId, content)
			res.status(201).json(message.messageId)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Set message open
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async open(req, res) {
		try {
			const { userId } = req.params
			const { messages } = req.body
			const message = await MessageService.open(userId, messages)
			res.json(message.messageId)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Delete a message
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async delete(req, res) {
		try {
			const { messageId } = req.params
			const message = await MessageService.delete(messageId)
			res.json(message.messageId)
		} catch (error) {
			handleError(res, error)
		}
	}
}

module.exports = MessageController

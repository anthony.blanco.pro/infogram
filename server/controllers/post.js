const GroupService = require('../services/group')
const PostService = require('../services/post')
const TagService = require('../services/tag')
const { handleError } = require('../utils/error')

class PostController {
	/**
	 * Get page of posts of user
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getPost(req, res) {
		try {
			const { userId } = req.params

			const { page, limit } = req.query

			const nbPage = page ? Number.parseInt(page) : 0
			const nbLimit = limit ? Number.parseInt(limit) : 25
			const posts = await PostService.getPage(userId, nbPage, nbLimit)
			res.json(posts)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get page of popular posts
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getPopularPost(req, res) {
		try {
			const { page, limit } = req.query
			const { until } = req.body
			const nbPage = page ? Number.parseInt(page) : 0
			const nbLimit = limit ? Number.parseInt(limit) : 25
			const posts = await PostService.getPopularPage(until, nbPage, nbLimit)
			res.json(posts)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get page of follower posts
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getFollowerPost(req, res) {
		try {
			const { userId } = req.params
			const { page, limit } = req.query
			const nbPage = page ? Number.parseInt(page) : 0
			const nbLimit = limit ? Number.parseInt(limit) : 25
			const posts = await PostService.getFollowerPage(userId, nbPage, nbLimit)
			res.json(posts)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get page of tag posts
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getTagsPost(req, res) {
		try {
			const { tagName } = req.params
			const { until } = req.body
			const { page, limit } = req.query
			const nbPage = page ? Number.parseInt(page) : 0
			const nbLimit = limit ? Number.parseInt(limit) : 25

			const tag = await TagService.getTagByName(tagName)
			if (tag) {
				const posts = await PostService.getTagsPost(
					tagName,
					until,
					nbPage,
					nbLimit
				)
				res.json(posts)
				return
			}
			res.send()
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get post for a group
	 *
	 * @param {Request} req
	 * @param {Response} res
	 * @returns Post[]
	 */
	static async getGroupPost(req, res) {
		try {
			const { groupName } = req.params
			const { until } = req.body
			const { page, limit } = req.query
			const nbPage = page ? Number.parseInt(page) : 0
			const nbLimit = limit ? Number.parseInt(limit) : 25

			const group = await GroupService.getById(groupName)
			if (group) {
				const posts = await PostService.getGroupPost(
					groupName,
					until,
					nbPage,
					nbLimit
				)
				res.json(posts)
				return
			}
			res.send()
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get page of children posts
	 *
	 * @param {Request} req
	 * @param {Reponse} res
	 */
	static async getResponse(req, res) {
		try {
			const { postId } = req.params

			const { page, limit } = req.query

			const nbPage = page ? Number.parseInt(page) : 0
			const nbLimit = limit ? Number.parseInt(limit) : 25

			const posts = await PostService.getResponse(postId, nbPage, nbLimit)
			res.json(posts)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get nb of children posts
	 *
	 * @param {Request} req
	 * @param {Reponse} res
	 */
	static async getNbResponse(req, res) {
		try {
			const { postId } = req.params

			const posts = await PostService.getNbResponse(postId)
			res.json(posts)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get post by his id
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getPostById(req, res) {
		try {
			const { postId } = req.params
			const post = await PostService.getPost(postId)
			res.json(post)
		} catch (error) {
			handleError(error)
		}
	}

	/**
	 * Create new post
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async create(req, res) {
		try {
			const { userId } = req.params
			const { description, content, type, parentId } = req.body
			const post = await PostService.create(
				userId,
				description,
				type,
				content,
				parentId
			)
			res.status(201).json(post)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Delete a post
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async delete(req, res) {
		try {
			const { postId } = req.params
			const post = await PostService.delete(postId)
			res.json(post)
		} catch (error) {
			handleError(res, error)
		}
	}
}

module.exports = PostController

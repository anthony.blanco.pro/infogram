const FollowerService = require('../services/follower')
const { handleError } = require('../utils/error')

class FollowerController {
	/**
	 * Get follower of user
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getFollower(req, res) {
		try {
			const { userId } = req.params
			const { page, limit } = req.query

			const nbPage = page ? Number.parseInt(page) : 0
			const nbLimit = limit ? Number.parseInt(limit) : 25
			const users = await FollowerService.getFollower(userId, nbPage, nbLimit)
			res.json(users)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get followed of user
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async getFollowed(req, res) {
		try {
			const { userId } = req.params
			const { page, limit } = req.query

			const nbPage = page ? Number.parseInt(page) : 0
			const nbLimit = limit ? Number.parseInt(limit) : 25
			const users = await FollowerService.getFollowed(userId, nbPage, nbLimit)
			res.json(users)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Is user followed by other
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async isFollowedBy(req, res) {
		try {
			const { followedId, followerId } = req.params
			const user = await FollowerService.isFollowedBy(followerId, followedId)
			res.json(user)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Get number of follower, followed
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async count(req, res) {
		try {
			const { userId } = req.params
			const count = await FollowerService.count(userId)
			res.json(count)
		} catch (error) {
			handleError(res, error)
		}
	}

	/**
	 * Create follower relation
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async create(req, res) {
		try {
			const { userId, followedId } = req.params
			const follow = await FollowerService.create(userId, followedId)
			res.json(follow)
		} catch (error) {
			handleError(res, error)
		}
	}
	/**
	 * Delete follower relation
	 *
	 * @param {Request} req
	 * @param {Response} res
	 */
	static async delete(req, res) {
		try {
			const { userId, followedId } = req.params
			const follow = await FollowerService.delete(userId, followedId)
			res.json(follow)
		} catch (error) {
			handleError(res, error)
		}
	}
}

module.exports = FollowerController

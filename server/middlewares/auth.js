const jwt = require('jsonwebtoken')
const MessageService = require('../services/message')
const GroupService = require('../services/group')
const PostService = require('../services/post')
const UserService = require('../services/user')
const { Role } = require('.prisma/client')

/**
 * Middleware for authentication
 *
 * @param {Request} req
 * @param {Response} res
 * @param {*} next
 */
module.exports.authenticateToken = (req, res, next) => {
	const token = req.cookies.jwt

	if (token == null) {
		res.locals.user = null
		return res.sendStatus(401)
	}

	jwt.verify(token, process.env.TOKEN_SECRET, async (err, userToken) => {
		const { userId } = req.params
		const user = await UserService.getById(userToken.userId)
		if (!err && (user.role === Role.ADMIN || user.role === Role.MODERATOR)) {
			res.locals.user = user
			next()
		} else {
			if (err || (userId && userId !== userToken.userId)) {
				return res.sendStatus(403)
			}
			res.locals.user = user
			next()
		}
	})
}

/**
 * Middleware for group owner
 *
 * @param {Request} req
 * @param {Response} res
 * @param {*} next
 */
module.exports.authenticateGroup = (req, res, next) => {
	const { groupName } = req.params
	const token = req.cookies.jwt

	if (token == null) {
		res.locals.user = null
		return res.sendStatus(401)
	}

	GroupService.getById(groupName).then(group => {
		const user = res.locals.user
		if (user.role === Role.ADMIN) {
			next()
		} else {
			if (group?.ownerId !== user.id) {
				return res.sendStatus(403)
			}
			next()
		}
	})
}

/**
 * Middleware for post owner
 *
 * @param {Request} req
 * @param {Response} res
 * @param {*} next
 */
module.exports.authenticatePost = (req, res, next) => {
	const { postId } = req.params
	const token = req.cookies.jwt

	if (token == null) {
		res.locals.user = null
		return res.sendStatus(401)
	}

	PostService.getPost(postId).then(post => {
		const user = res.locals.user
		if (user.role === Role.ADMIN) {
			next()
		} else {
			if (post?.publisherId !== user.id) {
				return res.sendStatus(403)
			}

			next()
		}
	})
}

/**
 * Middleware for message owner
 *
 * @param {Request} req
 * @param {Response} res
 * @param {*} next
 */
module.exports.authenticateMessage = (req, res, next) => {
	const { messageId } = req.params
	const token = req.cookies.jwt

	if (token == null) {
		res.locals.user = null
		return res.sendStatus(401)
	}

	MessageService.getMessage(messageId).then(message => {
		const user = res.locals.user
		if (user.role === Role.ADMIN) {
			next()
		} else {
			if (message?.emitterId !== user.id) {
				return res.sendStatus(403)
			}
			next()
		}
	})
}

/**
 * Middleware for filer badge
 *
 * @param {Request} req
 * @param {Response} res
 * @param {*} next
 */
module.exports.authenticateFiler = (req, res, next) => {
	if (!res.locals.user) {
		return res.sendStatus(401)
	}

	const folder = req.body.path
	const user = res.locals.user

	if (user.role === Role.ADMIN) {
		next()
	} else if (folder !== 'badge' && folder !== 'default') {
		next()
	} else {
		return res.sendStatus(403)
	}
}

/**
 * Middleware for admin
 *
 * @param {Request} req
 * @param {Response} res
 * @param {*} next
 * @returns
 */
module.exports.authenticateAdmin = (req, res, next) => {
	if (!res.locals.user) {
		return res.sendStatus(401)
	}

	const user = res.locals.user
	if (user.role === Role.ADMIN || user.role === Role.MODERATOR) {
		next()
	} else {
		return res.sendStatus(403)
	}
}

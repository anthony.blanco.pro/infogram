import clsx from 'clsx'

// MUI Imports
import {
	Box,
	Button,
	Divider,
	Grid,
	makeStyles,
	Paper,
	Tooltip,
	Typography,
} from '@material-ui/core'
import { ArrowBack } from '@material-ui/icons'

// Infogram components
import LoadingButton from '@/components/Buttons/LoadingButton/LoadingButton'

// Custom css style
const useStyles = makeStyles(theme => ({
	root: {
		padding: '1rem',
		width: '100%',
	},
	grid: {
		width: '100%',
	},
	divider: {
		marginTop: theme.spacing(1),
		marginBottom: theme.spacing(2),
	},
	center: {
		textAlign: 'center',
	},
	offsetRight: {
		paddingLeft: '2rem',
	},
}))

export default function PanelLayout({
	onReturn,
	onSubmit,
	submitIcon,
	submitTooltip,
	loading,
	title,
	className,
	children,
}) {
	const classes = useStyles()

	return (
		<Paper square elevation={1} className={clsx(classes.root, className)}>
			<Box>
				<Grid
					container
					direction="row"
					justify="space-between"
					alignItems="center"
					className={classes.grid}
				>
					<Grid item sm={2} className={Boolean(onSubmit) && classes.center}>
						<Tooltip title="BACK">
							<Button variant="outlined" color="secondary" onClick={onReturn}>
								<ArrowBack />
							</Button>
						</Tooltip>
					</Grid>
					<Grid
						item
						sm={8}
						className={
							(Boolean(onSubmit) && classes.center) || classes.offsetRight
						}
					>
						<Typography variant="h6">{title}</Typography>
					</Grid>
					{Boolean(onSubmit) && (
						<Grid item sm={2} className={classes.center}>
							<LoadingButton
								color="secondary"
								endIcon={submitIcon}
								tooltip={submitTooltip}
								variant="outlined"
								onClick={onSubmit}
								loading={loading}
							>
								{submitTooltip}
							</LoadingButton>
						</Grid>
					)}
				</Grid>
				<Divider className={classes.divider} />
			</Box>
			<Box className={classes.children}>{children}</Box>
		</Paper>
	)
}

import React from 'react'
import ReactDOM from 'react-dom'
import { CookiesProvider } from 'react-cookie'

import { AppProvider } from './contexts/AppContext'

import App from '@views/App'

import '@fonts/Hakuna-Sans.otf'
import '@fonts/Louis-George-Cafe.ttf'
import '@/index.css'

ReactDOM.render(
	<React.Fragment>
		<CookiesProvider>
			<AppProvider>
				<App />
			</AppProvider>
		</CookiesProvider>
	</React.Fragment>,
	document.getElementById('root')
)

import Login from '@views/Login/Login'
import Register from '@views/Register/Register'
import Home from '@views/Home/Home'
import NotFound from '@views/NotFound/NotFound'
import Popular from '@/views/Popular/Popular'
import Tag from './views/Tag/Tag'
import Account from './views/Account/Account'

const RouteAction = {
	DIRECT: 'DIRECT',
	REDIRECT: 'REDIRECT',
}

const Routes = [
	{
		name: '/',
		authenticated: false,
		granted: false,
		action: RouteAction.REDIRECT,
		redirectTo: '/home',
	},
	{
		name: '/login',
		authenticated: false,
		granted: false,
		action: RouteAction.DIRECT,
		component: Login,
	},
	{
		name: '/register',
		authenticated: false,
		granted: false,
		action: RouteAction.DIRECT,
		component: Register,
	},
	{
		name: '/home',
		authenticated: true,
		granted: false,
		action: RouteAction.DIRECT,
		component: Home,
		redirectTo: '/login',
	},
	{
		name: '/popular',
		authenticated: true,
		granted: false,
		action: RouteAction.DIRECT,
		component: Popular,
		redirectTo: '/login',
	},
	{
		name: '/tag',
		authenticated: true,
		granted: false,
		action: RouteAction.DIRECT,
		component: Tag,
		redirectTo: '/login',
	},
	{
		name: '/account',
		authenticated: true,
		granted: false,
		action: RouteAction.DIRECT,
		component: Account,
		redirectTo: '/login',
	},
	{
		name: '/post/:postId',
		authenticated: true,
		granted: false,
		action: RouteAction.DIRECT,
		component: Home,
		redirectTo: '/login',
	},
	{
		name: '/*',
		authenticated: false,
		granted: false,
		action: RouteAction.DIRECT,
		component: NotFound,
	},
]

export { Routes, RouteAction }

import { useContext, useState } from 'react'
import { useHistory } from 'react-router'

import {
	Button,
	Grid,
	makeStyles,
	Snackbar,
	TextField,
	Tooltip,
	Typography,
} from '@material-ui/core'
import { Check } from '@material-ui/icons'
import Alert from '@material-ui/lab/Alert'

// Infogram context
import AppContext from '@contexts/AppContext'

import BlankLayout from '@/layout/Blank'
import PanelLayout from '@/layout/Panel'

import DateField from '@/components/Fields/DateField/DateField'
import NicknameField from '@/components/Fields/NicknameField/NicknameField'
import EmailField from '@/components/Fields/EmailField/EmailField'
import PasswordField from '@/components/Fields/PasswordField/PasswordField'

import { UserService } from '@/services/user'

const useStyles = makeStyles(theme => ({
	layout: {
		marginTop: theme.spacing(3),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		width: '100%',
		height: '90vh',
		'& > div, & > div > div': {
			height: 'inherit',
		},
	},
	panel: {
		marginTop: '15px',
		height: '100%',
		alignItems: 'center',
	},
	content: {
		height: '100%',
		width: '600px',
		'& > *': {
			display: 'flex',
			alignItems: 'center',
			justifyContent: 'space-between',
		},
	},
	gridElem: {
		margin: '10px',
	},
	field: {
		width: '50vh',
		paddingRight: '15px',
	},
	date: {
		paddingRight: '15px',
	},
	passwordContent: {
		marginTop: '50px',
	},
	label: {
		textAlign: 'center',
	},
	delete: {
		marginTop: '10px',
		color: theme.palette.error.dark,
		background: theme.palette.background.default,
	},
	separator: {
		height: '100px',
		alignItems: 'center',
		display: 'flex',
		justifyContent: 'center',
	},
}))

export default function Account() {
	const classes = useStyles()
	const history = useHistory()
	const appContext = useContext(AppContext)
	const user = appContext.value.user

	const [nickname, setNickname] = useState(undefined)
	const [email, setEmail] = useState(undefined)
	const [phone, setPhone] = useState(undefined)
	const [birth, setBirth] = useState(undefined)
	const [password, setPassword] = useState(undefined)

	const [alertValue, setAlertValue] = useState(null)
	const [text, setText] = useState(null)

	function handleClose() {
		history.go(-1)
	}

	function handleSubmit() {
		UserService.updateUser(user.id, {
			nickname: nickname,
			email: email,
			phone: phone,
			birth: new Date(birth ?? 0).toISOString(),
		}).then(res => {
			if (res.code) {
				console.log(res)
				if (res.code === 'P2002')
					setAlertValue(
						`Already used : ${res.meta.target.map(meta => `${meta} `)}`
					)
				else setAlertValue('You get rick rolled')
			} else {
				password &&
					UserService.changePassword(user.id, password).then(res => {
						if (res.code) {
							setAlertValue('You get rick rolled')
						}
					})

				setText('Updated successfuly')
			}
		})
	}

	/**
	 * Handle delete button
	 */
	function handleDelete() {
		UserService.deleteAccount(user.id).then(res => {
			if (res.code) setAlertValue('You get rick rolled')
			else {
				setText('Goodbye')
				setTimeout(() => {
					UserService.logout(appContext)
					history.push('/')
				}, 1500)
			}
		})
	}

	return (
		<BlankLayout loading={false} className={classes.layout}>
			<PanelLayout
				title={'Edit profile'}
				submitIcon={<Check color="secondary" />}
				submitTooltip="APPLY"
				onReturn={handleClose}
				onSubmit={handleSubmit}
				className={classes.panel}
			>
				{user && (
					<>
						<Grid
							className={classes.content}
							container
							direction="column"
							justify="flex-start"
							alignItems="stretch"
						>
							<Grid container item className={classes.gridElem}>
								<Grid item>
									<Typography>Nickname</Typography>
								</Grid>
								<Grid item>
									<NicknameField
										defaultValue={user.nickname}
										onChange={e => setNickname(e.target.value)}
										className={classes.field}
									/>
								</Grid>
							</Grid>
							<Grid container item className={classes.gridElem}>
								<Grid item>
									<Typography>Email</Typography>
								</Grid>
								<Grid item>
									<EmailField
										defaultValue={user.email}
										onChange={e => setEmail(e.target.value)}
										className={classes.field}
									/>
								</Grid>
							</Grid>
							<Grid container item className={classes.gridElem}>
								<Grid item>
									<Typography>Phone</Typography>
								</Grid>
								<Grid item>
									<TextField
										defaultValue={user.phone}
										onChange={e => setPhone(e.target.value)}
										className={classes.field}
									/>
								</Grid>
							</Grid>
							<Grid item className={classes.gridElem}>
								<DateField
									value={birth ? birth : new Date(user.birth ?? 0)}
									label={'Date of birth'}
									onChange={date => setBirth(date)}
									className={classes.date}
								/>
							</Grid>
							<Grid container item className={classes.gridElem}>
								<Grid item>
									<Typography>Password</Typography>
								</Grid>
								<Grid item className={classes.field}>
									<PasswordField
										visibleToggleable={true}
										onChange={e => setPassword(e.target.value)}
									/>
								</Grid>
							</Grid>
							<Grid item className={classes.gridElem}>
								<Tooltip title="Delete permanently your account">
									<Button
										variant="contained"
										className={classes.delete}
										onClick={() => handleDelete()}
									>
										Delete
									</Button>
								</Tooltip>
							</Grid>
						</Grid>
					</>
				)}
				{alertValue && (
					<Alert
						variant="outlined"
						severity="error"
						onClose={() => {
							setAlertValue(null)
						}}
						className={classes.alert}
					>
						{alertValue}
					</Alert>
				)}
				<Snackbar
					open={Boolean(text)}
					autoHideDuration={1500}
					onClose={handleClose}
				>
					<Alert onClose={handleClose} severity="info">
						{text}
					</Alert>
				</Snackbar>
			</PanelLayout>
		</BlankLayout>
	)
}

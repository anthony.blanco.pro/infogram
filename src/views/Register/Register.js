import { useState } from 'react'
import { useHistory } from 'react-router'

// MUI core Imports
import { Button, Divider, Link, makeStyles } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'

// Infogram services
import { UserService } from '@services/user'

// Infogram layouts
import BlankLayout from '@/layout/Blank'

// Infogram components
import PasswordField from '@components/Fields/PasswordField/PasswordField'
import EmailField from '@components/Fields/EmailField/EmailField'
import Namefield from '@components/Fields/NameField/NameField'
import NicknameField from '@components/Fields/NicknameField/NicknameField'
import DateField from '@components/Fields/DateField/DateField'

// Custom css style
const useStyles = makeStyles(theme => ({
	layout: {
		marginTop: theme.spacing(3),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		width: '100%',
	},
	form: {
		width: '35rem',
		marginTop: theme.spacing(2),
	},
	alert: {
		width: '100%',
		marginTop: theme.spacing(4),
	},
	submit: {
		margin: theme.spacing(4, 0, 2),
	},
	link: {
		textDecoration: 'none',
	},
	cardContent: {
		padding: theme.spacing(5),
	},
	divider: {
		width: '100%',
		margin: theme.spacing(1, 0, 2),
	},
	backdrop: {
		zIndex: theme.zIndex.drawer + 1,
		color: '#fff',
	},
	input: {
		marginTop: '1rem',
	},
}))

/**
 * Register view
 */
export default function Register() {
	const classes = useStyles()
	const history = useHistory()

	const [alertValue, setAlertValue] = useState('')
	const [isLoading, setLoading] = useState(false)

	const [emailInput, setEmailValue] = useState('')
	const [firstnameInput, setFirstNameValue] = useState('')
	const [lastnameInput, setLastNameValue] = useState('')
	const [nicknameInput, setNicknameValue] = useState('')
	const [birthdateInput, setBirthdateValue] = useState()
	const [passwordInput, setPasswordValue] = useState('')
	const [passwordConfirmInput, setPasswordConfirmValue] = useState('')

	function handleFormSubmit(event) {
		event.preventDefault()

		if (
			Boolean(emailInput) &&
			Boolean(firstnameInput) &&
			Boolean(lastnameInput) &&
			Boolean(nicknameInput) &&
			Boolean(birthdateInput) &&
			Boolean(passwordInput) &&
			passwordInput.length >= 8 &&
			Boolean(passwordConfirmInput) &&
			passwordInput === passwordConfirmInput
		) {
			setLoading(true)

			// Login user with backend : Return a jwt token automaticlly saved by the web browser
			UserService.register(
				emailInput,
				`${firstnameInput} ${lastnameInput}`,
				nicknameInput,
				birthdateInput,
				passwordInput
			)
				.then(() => {
					setLoading(false)
					history.push('/login') // TODO : Email verification
				})
				.catch(err => {
					console.error(err)
					setAlertValue(
						'Something went wrong when we were trying to create your account. Please, try again later.'
					)
					setLoading(false)
				})
		} else {
			setAlertValue(
				'One or more fields are invalid or incomplete. Please check it and try again.'
			)
		}
	}

	return (
		<BlankLayout loading={isLoading} className={classes.layout}>
			{Boolean(alertValue) && (
				<Alert
					variant="outlined"
					severity="error"
					onClose={() => {
						setAlertValue('')
					}}
					className={classes.alert}
				>
					{alertValue}
				</Alert>
			)}

			<form className={classes.form} noValidate onSubmit={handleFormSubmit}>
				{/* Email */}
				<EmailField
					id="email-input"
					label="Email"
					onChange={event => setEmailValue(event.target.value)}
				/>

				{/* Name */}
				<Namefield
					onFNameChange={event => setFirstNameValue(event.target.value)}
					onLNameChange={event => setLastNameValue(event.target.value)}
				/>

				{/* Nickname */}
				<NicknameField
					id="nickname"
					onChange={event => setNicknameValue(event.target.value)}
				/>

				{/* Date picker */}
				<DateField
					id="birthdate"
					label="Birthdate"
					onChange={date => setBirthdateValue(date)}
				/>

				{/* Password */}
				<PasswordField
					id="password-field"
					label="Password"
					onChange={event => setPasswordValue(event.target.value)}
					visibleToggleable={true}
					autoComplete="new-password"
				/>

				{/* Password Confirm */}
				<PasswordField
					id="password-confirm-field"
					label="Password Confirmation"
					onChange={event => setPasswordConfirmValue(event.target.value)}
					visibleToggleable={false}
					autoComplete="new-password"
				/>

				<Button
					type="submit"
					fullWidth
					variant="contained"
					color="secondary"
					className={classes.submit}
				>
					Register
				</Button>
			</form>

			<Divider orientation="horizontal" className={classes.divider} />

			<Link
				href="/login"
				variant="body1"
				color="secondary"
				className={classes.link}
			>
				You already have an account ? Log in
			</Link>
		</BlankLayout>
	)
}

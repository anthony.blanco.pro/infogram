import axios from 'axios'
import jwt_decode from 'jwt-decode'

/**
 * Get user token. Return null if token doesn't exist
 *
 * @returns User token
 */
function getJwtToken() {
	let ca = document.cookie.split(';')
	for (let i = 0; i < ca.length; i++) {
		let c = ca[i].trim().split('=')
		if (c[0] === 'jwt') {
			return c[1]
		}
	}
	return null
}

/**
 * Return the current user
 *
 * @param {React.Context} appContext
 * @returns User
 */
function getUser(appContext) {
	return appContext.value.user
}

/**
 * Return the current user id
 */
function getUserId() {
	return jwt_decode(UserService.getJwtToken()).userId
}

/**
 * Login user. Jwt credential is automatically saved
 *
 * @param {*} login user email or phonenumber
 * @param {*} password user password
 * @returns Promise with user or error
 */
function login(appContext, login, password) {
	return axios
		.post(
			`/api/user/login`,
			{
				login: login,
				password: password, // Must be encrypted (SHA512/MD5)
			},
			{
				headers: {
					'content-type': 'application/json',
				},
				withCredentials: true,
			}
		)
		.then(res => {
			console.debug(res)
			UserService.store(appContext)
		})
		.catch(err => {
			console.error(err)
		})
}

/**
 * Logout clear user token and app context
 */
function logout(appContext) {
	let d = new Date(0)
	let expires = 'expires=' + d.toUTCString()
	appContext.setValue({
		user: null,
	})
	document.cookie = 'jwt=;' + expires + ';path=/'
}

/**
 * Register a new user
 *
 * @param {String} email Email of user. Must be unique
 * @param {String} name Name of user.
 * @param {String} nickname Nickname of user. Must be unique
 * @param {String} birthdate Birthday of user.
 * @param {String} password Password of user account
 * @returns Promise if account is registred
 */
function register(email, name, nickname, birthdate, password) {
	return axios
		.post(
			`/api/user/register`,
			{
				user: {
					email: email,
					name: name,
					birthdate: birthdate,
					nickname: nickname,
				},
				password: password,
			},
			{
				headers: {
					'content-type': 'application/json',
				},
			}
		)
		.then(res => {
			console.debug(res)
		})
		.catch(err => {
			console.error(err)
		})
}

/**
 * Store function get user data from backend server by using jwt token and store theme in appContext
 *
 * @param {any} appContext
 */
function store(appContext) {
	let token = UserService.getJwtToken()
	let decoded = jwt_decode(token)
	getUserDetail(decoded.userId)
		.then(user => {
			appContext.setValue({
				user: user,
			})
		})
		.catch(err => {
			console.error(err)
		})
}

/**
 * Update an user
 *
 * @param {string} userId
 * @param {User} user
 * @returns User
 */
function updateUser(userId, user) {
	return axios
		.put(
			`/api/user/${userId}`,
			{
				user: user,
			},
			{ withCredentials: true }
		)
		.then(res => res.data)
		.catch(err => err.response.data)
}

/**
 * Delete an user
 *
 * @param {string} userId
 * @returns User
 */
function deleteAccount(userId) {
	return axios
		.delete(`/api/user/${userId}`, { withCredentials: true })
		.then(res => res.data)
		.catch(err => err.response.data)
}

function changePassword(userId, password) {
	return axios
		.post(
			`/api/user/${userId}/change-password`,
			{
				password: password,
			},
			{ withCredentials: true }
		)
		.then(res => res.data)
		.catch(err => err.response.data)
}

/**
 * Update a profile
 *
 * @param {string} userId
 * @param {User} user
 * @returns User
 */
function updateProfile(userId, description) {
	return axios
		.put(
			`/api/user/profile/${userId}`,
			{
				description: description,
			},
			{ withCredentials: true }
		)
		.then(res => res.data)
		.catch(err => err.response.data)
}

/**
 * Get user detail (nickname, ...)
 *
 * @param {string} userId
 * @returns User
 */
function getUserDetail(userId) {
	return axios
		.get(`/api/user/${userId}`, { withCredentials: true })
		.then(res => res.data)
		.catch(err => err)
}

/**
 * Get user profile
 *
 * @param {string} userId
 * @returns Profile
 */
function getUserProfile(userId) {
	return axios
		.get(`/api/user/profile/${userId}`, { withCredentials: true })
		.then(res => res.data)
		.catch(err => err)
}

/**
 * Get user favorite
 *
 * @param {string} userId
 * @param {number} page
 * @returns Favorite
 */
function getFavorites(userId, page) {
	return axios
		.get(`/api/user/favorite/${userId}?page=${page}`, { withCredentials: true })
		.then(res => res.data)
		.catch(err => err)
}
/*
 * Get the number of followers and following for someone
 *
 * @param {string} userId
 * @returns int
 */
function getUserFollowerCount(userId) {
	return axios
		.get(`/api/user/follower/count/${userId}`, { withCredentials: true })
		.then(res => res.data)
		.catch(err => err)
}

/**
 * Subscribe to another user
 *
 * @param {string} selfUserId
 * @param {string} followedUserId
 * @returns
 */
function subscribeToUser(selfUserId, followedUserId) {
	return axios
		.post(`/api/user/follower/${selfUserId}/${followedUserId}`, {
			withCredentials: true,
		})
		.then(res => res.data)
		.catch(err => err)
}

/*
 * Unsubscribe to another user
 *
 * @param {string} followerId
 * @param {string} followedId
 * @returns
 */
function unsubscribeToUser(followerId, followedId) {
	return axios
		.delete(`/api/user/follower/${followerId}/${followedId}`, {
			withCredentials: true,
		})
		.then(res => res.data)
		.catch(err => err)
}

/**
 * Add new favorite to user
 *
 * @param {string} userId
 * @param {string} postId
 * @returns Favorite
 */
function addFavorite(userId, postId) {
	return axios
		.post(`/api/user/favorite/${userId}/${postId}`, { withCredentials: true })
		.then(res => res.data)
		.catch(err => err)
}

/**
 * Get favorite by ids
 *
 * @param {string} userId
 * @param {string} postId
 * @returns Favorite
 */
function getSpecificFavorite(userId, postId) {
	return axios
		.get(`/api/user/favorite/${userId}/${postId}`, { withCredentials: true })
		.then(res => res.data)
		.catch(err => err)
}

/*
 * checks if user is followed by
 *
 * @param {string} followerId
 * @param {string} followedId
 * @returns user||null
 */
function isFollowedBy(followerId, followedId) {
	return axios
		.get(`/api/user/follower/${followerId}/${followedId}`, {
			withCredentials: true,
		})
		.then(res => res.data)
		.catch(err => err)
}

/**
 * Remove favorite to user
 *
 * @param {string} userId
 * @param {string} postId
 * @returns Favorite
 */
function removeFavorite(userId, postId) {
	return axios
		.delete(`/api/user/favorite/${userId}/${postId}`, {
			withCredentials: true,
		})
		.then(res => res.data)
		.catch(err => err)
}

export const UserService = {
	getJwtToken,
	getUser,
	getUserId,
	login,
	logout,
	register,
	store,

	updateUser,
	updateProfile,
	changePassword,
	deleteAccount,

	getUserDetail,
	getUserProfile,
	getFavorites,
	getSpecificFavorite,
	getUserFollowerCount,

	addFavorite,
	removeFavorite,
	subscribeToUser,
	isFollowedBy,
	unsubscribeToUser,
}

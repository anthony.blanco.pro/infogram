/* eslint-disable react-hooks/exhaustive-deps */
import { Box, Button, Grid, makeStyles, Paper } from '@material-ui/core'
import { useContext, useEffect, useState } from 'react'

import { NumberService } from '@services/number'
import { UserService } from '@services/user'

// Infogram context
import AppContext from '@contexts/AppContext'
import EditProfile from './Edit/EditProfile.js'

// Custom css style
const useStyles = makeStyles(theme => ({
	boxProfileInfo: {
		padding: '8px 100px',
		width: '100%',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	follow: {
		fontSize: '1rem',
		color: theme.palette.text.secondary,
		padding: '0 0 0 4px',
	},
	at: {
		fontSize: '1.2rem',
		color: theme.palette.text.secondary,
	},
	createdAt: {
		fontSize: '0.8rem',
		color: theme.palette.text.secondary,
		fontWeight: 'light',
	},
	number: {
		maxWidth: '40px',
		minWidth: '40px',
		fontSize: '1.1rem',
		fontWeight: 'bold',
	},
	description: {
		padding: '5px 20px 50px 100px',
	},
	boxButton: {
		direction: 'rtl',
		padding: '10px 100px 0 0',
	},
	followGrid: {
		textAlign: 'right',
	},
}))

export default function ProfileInfo({ profileUser, me, profile }) {
	const classes = useStyles()
	const appContext = useContext(AppContext)
	const user = appContext.value.user

	const [isFollowedBy, setIsFollowedBy] = useState(null)
	const [follow, setFollow] = useState(null)
	const [isLoading, setIsLoading] = useState(false)
	const [open, setOpen] = useState(false)

	useEffect(() => {
		if (user) {
			fetchData()
		}
	}, [profileUser, user])

	function fetchData() {
		UserService.getUserFollowerCount(profileUser.id).then(res => {
			setFollow(res)
		})
		if (!me) {
			UserService.isFollowedBy(user.id, profileUser.id).then(res =>
				setIsFollowedBy(res)
			)
		}
		setIsLoading(false)
	}

	useEffect(() => {
		fetchData()
	}, [isLoading])

	function handleButton() {
		if (me) {
			return <Button onClick={() => setOpen(true)}>Edit Profile</Button>
		} else {
			return handleSubButton()
		}
	}

	function handleSubButton() {
		return isFollowedBy ? (
			<Button
				variant="contained"
				color="secondary"
				onClick={() => {
					UserService.unsubscribeToUser(user.id, profileUser.id).then(res => {
						setIsLoading(true)
					})
				}}
			>
				Subscribed
			</Button>
		) : (
			<Button
				variant="outlined"
				color="secondary"
				onClick={() => {
					UserService.subscribeToUser(user.id, profileUser.id).then(res => {
						setIsLoading(true)
					})
				}}
			>
				Subscribe
			</Button>
		)
	}

	return (
		<Paper>
			{!isLoading && (
				<>
					<Box className={classes.boxButton}>
						{profileUser && handleButton()}
					</Box>

					<Box className={classes.boxProfileInfo}>
						<Grid>
							<Box>
								<span className={classes.at}>@{profileUser?.nickname}</span>
							</Box>
							<Box>
								<span> member since : </span>
								<span className={classes.createdAt}>
									{new Date(profileUser?.createdAt ?? 0).toLocaleDateString()}
								</span>
							</Box>
						</Grid>
						<Grid className={classes.followGrid}>
							<Box>
								<span className={classes.number}>
									{NumberService.format(follow?.followed ?? 0, 2)}
								</span>
								<span className={classes.follow}>Followers</span>
							</Box>
							<Box>
								<span className={classes.number}>
									{NumberService.format(follow?.follower ?? 0, 2)}
								</span>
								<span className={classes.follow}>Following</span>
							</Box>
						</Grid>
					</Box>
					<Box className={classes.description}>{profile?.description}</Box>
					<EditProfile
						open={open}
						setOpen={setOpen}
						profile={profile}
						user={profileUser}
					/>
				</>
			)}
		</Paper>
	)
}

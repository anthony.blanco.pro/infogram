/* eslint-disable react-hooks/exhaustive-deps */
import { useContext, useEffect, useState } from 'react'

// Material UI Import
import { Avatar, Box, Card, makeStyles, Typography } from '@material-ui/core'

// Infogram context
import AppContext from '@contexts/AppContext'

//Infograme services
import { UserService } from '@services/user'
import ProfileInfo from './ProfileInfo'

// Custom css style
const useStyles = makeStyles(theme => ({
	boxBackground: {
		height: '300px',
		backgroundRepeat: 'no-repeat',
		backgroundSize: 'cover',
	},
	avatarContainer: {
		zIndex: 10,
		overflow: 'hidden',
		position: 'relative',
		display: 'flex',
		alignItems: 'flex-end',
		top: '175px',
		background: 'linear-gradient(to top, #323232 10%, rgba(0,0,0,0))',
		padding: '0 0 8px 100px',
	},
	avatar: {
		background: theme.palette.text.primary,
		borderRadius: '100%',
		width: '125px',
		height: '125px',
	},
	name: {
		paddingLeft: '0.8rem',
		fontWeight: 'bold',
	},
}))

export default function ProfileHead({ userId }) {
	const classes = useStyles()
	const appContext = useContext(AppContext)
	const user = appContext.value.user

	//useState
	const [profile, setProfile] = useState(null)
	const [stateUser, setStateUser] = useState(null)
	const [isLoading, setIsLoading] = useState(false)
	const [me, setMe] = useState(false)

	useEffect(() => {
		setIsLoading(true)
		if (user) {
			if (!userId || user.id === userId) {
				setMe(true)
				setStateUser(user)
			} else {
				setMe(false)
				UserService.getUserDetail(userId).then(res => setStateUser(res))
			}
		}
	}, [user])

	useEffect(() => {
		if (stateUser) {
			UserService.getUserProfile(stateUser.id).then(res => {
				setProfile(res)
				setIsLoading(false)
			})
		}
	}, [stateUser])

	return (
		<Card variant="elevation">
			{!isLoading && (
				<>
					<Box
						className={classes.boxBackground}
						style={{
							backgroundImage: `url(/api/filer/download/${encodeURIComponent(
								profile?.backgroundImage ?? 'default/background.png'
							)})`,
						}}
					>
						<Box className={classes.avatarContainer}>
							<Avatar
								className={classes.avatar}
								src={`/api/filer/download/${encodeURIComponent(
									profile?.image ?? 'default/profile.png'
								)}`}
							/>
							<Typography variant="h3" className={classes.name}>
								{stateUser?.name}
							</Typography>
						</Box>
					</Box>
					{stateUser && (
						<ProfileInfo profileUser={stateUser} me={me} profile={profile} />
					)}
				</>
			)}
		</Card>
	)
}

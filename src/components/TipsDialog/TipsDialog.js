import { useState } from 'react'

// MUI Imports
import { Info } from '@material-ui/icons'
import {
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogContentText,
	DialogTitle,
	IconButton,
	makeStyles,
} from '@material-ui/core'

// Custom css style
const useStyles = makeStyles(theme => ({
	button: {
		padding: '0px 5px',
		margin: 0,
	},
}))

export default function TipsDialog({
	title,
	children,
	className = {},
	...props
}) {
	const classes = useStyles()
	const [open, setOpen] = useState(false)

	function handleClickOpen() {
		setOpen(true)
	}

	function handleClose() {
		setOpen(false)
	}

	return (
		<div className={className} {...props}>
			<IconButton
				color="secondary"
				onClick={handleClickOpen}
				className={classes.button}
			>
				<Info />
			</IconButton>
			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
			>
				<DialogTitle id="alert-dialog-title">{title}</DialogTitle>
				<DialogContent>
					<DialogContentText id="alert-dialog-description">
						{children}
					</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose} color="secondary" autoFocus>
						Close
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	)
}

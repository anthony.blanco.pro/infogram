import { createRef, useContext, useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'

import { Avatar, Button, Menu, MenuItem } from '@material-ui/core'
import AppContext from '@/contexts/AppContext'
import { UserService } from '@/services/user'

const useStyles = makeStyles(theme => ({
	menu: {
		top: '75px',
	},
	menuSpacer: {
		marginLeft: '0.35rem',
		width: theme.spacing(3),
		height: theme.spacing(3),
		backgroundColor: theme.palette.text.primary,
	},
	menuButton: {
		textTransform: 'lowercase',
	},
}))

export default function DropDownButton({ ariaLabel, menuItems, children }) {
	const classes = useStyles()

	const [anchorEl, setAnchorEl] = useState(null)
	const [profile, setProfile] = useState(null)

	const profilMenuRef = createRef()

	const appContext = useContext(AppContext)
	const user = appContext.value.user

	useEffect(() => {
		UserService.getUserProfile(user.id).then(res => setProfile(res))
	}, [user.id])

	function handleProfileMenuOpen(event) {
		setAnchorEl(event.currentTarget)
	}

	function handleProfileMenuClose() {
		setAnchorEl(null)
	}

	return (
		<div>
			<Button
				edge="end"
				aria-label={ariaLabel}
				aria-controls={profilMenuRef}
				aria-haspopup="true"
				onClick={handleProfileMenuOpen}
				className={classes.menuButton}
			>
				{children}
				<Avatar
					className={classes.menuSpacer}
					src={`/api/filer/download/${encodeURIComponent(
						profile?.image ?? 'default/profile.png'
					)}`}
				/>
			</Button>

			<Menu
				ref={profilMenuRef}
				anchorEl={anchorEl}
				getContentAnchorEl={null}
				anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
				transformOrigin={{ vertical: 'top', horizontal: 'center' }}
				elevation={0}
				keepMounted
				open={Boolean(anchorEl)}
				onClose={handleProfileMenuClose}
			>
				{menuItems.map(({ name, onClick }, idx) => {
					return (
						<MenuItem onClick={onClick} key={idx}>
							{name}
						</MenuItem>
					)
				})}
			</Menu>
		</div>
	)
}

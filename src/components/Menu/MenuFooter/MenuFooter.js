import { Link } from 'react-router-dom'

// MUI Imports
import { Box, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
	menuFooter: {
		textAlign: 'center',
		paddingTop: '4rem',
	},
	menuFooterLink: {
		textDecoration: 'underline',
		fontWeight: '700',
		color: theme.palette.text.secondary,
	},
}))

export default function MenuFooter() {
	const classes = useStyles()
	return (
		<Box className={classes.menuFooter}>
			<Typography variant="body2">
				&copy; {new Date().getFullYear()} Copyleft:{' '}
				<Link to="/" className={classes.menuFooterLink}>
					infogram.com
				</Link>
			</Typography>
		</Box>
	)
}

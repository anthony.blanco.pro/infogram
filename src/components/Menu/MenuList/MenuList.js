// MUI Imports
import { List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

// MUI Icons
import StarIcon from '@material-ui/icons/Star'
import RssFeedIcon from '@material-ui/icons/RssFeed'
import PeopleIcon from '@material-ui/icons/People'
import { useHistory } from 'react-router'

const useStyles = makeStyles(theme => ({
	menu: {
		flexGrow: 1,
		overflow: 'hidden',
	},
	listBtn: {
		paddingLeft: '2rem',
	},
	text: {
		marginLeft: '0.7rem',
	},
}))

export default function MenuList() {
	const classes = useStyles()
	const history = useHistory()

	const menuList = [
		{
			name: 'Popular',
			onClick: () => history.push('/home'),
			icon: <StarIcon />,
		},
		{
			name: 'Subscriptions',
			onClick: () => history.push('/subs'),
			icon: <RssFeedIcon />,
		},
		{
			name: 'Groups',
			onClick: () => history.push('/home'),
			icon: <PeopleIcon />,
		},
	]

	return (
		<List component="nav" aria-label="menu-list" className={classes.menu}>
			{menuList.map((menu, idx) => (
				<ListItem
					key={idx}
					className={classes.listBtn}
					button
					onClick={menu.onClick}
				>
					<ListItemIcon>{menu.icon}</ListItemIcon>
					<ListItemText primary={menu.name} className={classes.text} />
				</ListItem>
			))}
		</List>
	)
}

import { useState } from 'react'

import { FormHelperText, Grid, TextField } from '@material-ui/core'

export default function Namefield({ onFNameChange, onLNameChange, ...props }) {
	const [firstnameInput, setFirstNameInputValue] = useState('')
	const [lastnameInput, setLastNameInputValue] = useState('')

	const [firstnameError, setFirstNameError] = useState('')
	const [lastnameError, setLastNameError] = useState('')

	// Verify FirstName input
	function handleFirstNameInputChange(event) {
		setFirstNameInputValue(event.target.value)
		if (event.target.value.match('^$')) {
			setFirstNameError('Firstname cannot be empty.')
		} else if (!event.target.value.match('^[a-zA-Z]+$')) {
			setFirstNameError('Firstname contain invalid charaters.')
		} else {
			setFirstNameError('')
		}

		// onChange Callback
		onFNameChange(event)
	}

	// Verify LastName input
	function handleLastNameInputChange(event) {
		setLastNameInputValue(event.target.value)
		if (event.target.value.match('^$')) {
			setLastNameError('Lastname cannot be empty.')
		} else if (!event.target.value.match('^[a-zA-Z]+$')) {
			setLastNameError('Lastname contain invalid charaters.')
		} else {
			setLastNameError('')
		}

		// onChange Callback
		onLNameChange(event)
	}

	return (
		<Grid container spacing={3}>
			{/* FirstName */}
			<Grid item xs={6}>
				<TextField
					margin="normal"
					required
					error={Boolean(firstnameError)}
					fullWidth
					color="secondary"
					label="Firstname"
					id="firstname"
					value={firstnameInput}
					onChange={handleFirstNameInputChange}
					autoComplete="given-name"
					{...props}
				/>
				<FormHelperText error>{firstnameError}</FormHelperText>
			</Grid>

			{/* LastName */}
			<Grid item xs={6}>
				<TextField
					margin="normal"
					required
					error={Boolean(lastnameError)}
					fullWidth
					color="secondary"
					label="Lastname"
					id="lastname"
					value={lastnameInput}
					onChange={handleLastNameInputChange}
					autoComplete="family-name"
					{...props}
				/>
				<FormHelperText error>{lastnameError}</FormHelperText>
			</Grid>
		</Grid>
	)
}

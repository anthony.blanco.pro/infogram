import clsx from 'clsx'
import { makeStyles, TextField } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
	textarea: {
		width: '100%',
	},
}))

export default function TextAreaField({
	id,
	label,
	value,
	onChange,
	placeholder = '',
	className = {},
	multiline = true,
	variant = 'outlined',
	rows = 8,
	rowsMax = 10,
	...props
}) {
	const classes = useStyles()

	return (
		<TextField
			id={id}
			label={label}
			multiline={multiline}
			rows={rows}
			rowsMax={rowsMax}
			placeholder={placeholder}
			variant={variant}
			color="secondary"
			value={value}
			onChange={onChange}
			className={clsx(classes.textarea, className)}
			{...props}
		/>
	)
}

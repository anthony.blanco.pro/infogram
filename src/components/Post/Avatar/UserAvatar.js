import { useEffect, useState } from 'react'
import { useHistory } from 'react-router'
import { Avatar, Button, CardHeader, makeStyles } from '@material-ui/core'

// Infogram services
import { UserService } from '@/services/user'

// Custom css style
const useStyles = makeStyles(theme => ({
	button: {
		textTransform: 'inherit',
		padding: '0',
	},
	avatar: {
		width: theme.spacing(3),
		height: theme.spacing(3),
		backgroundColor: theme.palette.text.primary,
	},
	user: {
		marginLeft: '10px',
	},
}))

export default function UserAvatar({ userId }) {
	const classes = useStyles()
	const history = useHistory()

	const [user, setUser] = useState(null)
	const [profile, setProfile] = useState(null)

	useEffect(() => {
		UserService.getUserDetail(userId).then(res => setUser(res))
		UserService.getUserProfile(userId).then(res => setProfile(res))
	}, [userId])

	return (
		<Button
			className={classes.button}
			onClick={() => history.push(`/profile/${userId}`)}
		>
			{profile && (
				<CardHeader
					avatar={
						<Avatar
							className={classes.avatar}
							src={`/api/filer/download/${encodeURIComponent(
								profile?.image ?? 'default/profile.png'
							)}`}
						/>
					}
					title={`@${user?.nickname ?? 'undefined'}`}
					className={classes.user}
				/>
			)}
		</Button>
	)
}

import { useEffect, useState } from 'react'
import { AvatarGroup } from '@material-ui/lab'
import { Avatar, Badge, makeStyles } from '@material-ui/core'

import { PostService } from '@/services/post'

// Custom css style
const useStyles = makeStyles(theme => ({
	root: {
		height: theme.spacing(4),
		'& > div': {
			width: '30px',
			height: '30px',
		},
		'& > *': {
			fontSize: '1rem',
		},
	},
	badge: {
		marginRight: '10px',
		'& > *': {
			fontSize: '0.6rem',
			width: theme.spacing(2),
			height: theme.spacing(2),
		},
	},
	avatar: {
		width: theme.spacing(3),
		height: theme.spacing(3),
		backgroundColor: theme.palette.text.primary,
	},
}))

export default function BadgeAvatar({ postId }) {
	const classes = useStyles()

	const [badges, setBadges] = useState([])
	const [badgePost, setBadgePost] = useState([])

	useEffect(() => {
		PostService.getBadges().then(res => setBadges(res))
		PostService.getBadgePost(postId).then(res => setBadgePost(res))
	}, [postId])

	return (
		<AvatarGroup max={3} className={classes.root}>
			{badgePost.map(
				badge =>
					badge.count > 0 && (
						<Badge
							badgeContent={badge.count === 1 ? badge.count - 1 : badge.count}
							max={99}
							key={badge.name}
							color="primary"
							anchorOrigin={{
								vertical: 'bottom',
								horizontal: 'right',
							}}
							className={classes.badge}
						>
							<Avatar
								className={classes.avatar}
								src={
									`/api/filer/download/` +
									encodeURIComponent(
										badges.find(bdg => bdg.name === badge.name)?.icon
									)
								}
							/>
						</Badge>
					)
			)}
		</AvatarGroup>
	)
}

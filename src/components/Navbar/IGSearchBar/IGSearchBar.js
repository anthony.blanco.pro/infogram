import { useState } from 'react'
import { useHistory } from 'react-router'

// MUI core Imports
import { fade, makeStyles } from '@material-ui/core/styles'
import { InputBase } from '@material-ui/core'

// MUI Icons
import SearchIcon from '@material-ui/icons/Search'

// Custom css style
const useStyles = makeStyles(theme => ({
	search: {
		position: 'relative',
		borderRadius: theme.shape.borderRadius,
		backgroundColor: fade(theme.palette.common.white, 0.15),
		transition: '0.3s',
		'&:hover': {
			backgroundColor: fade(theme.palette.common.white, 0.25),
		},
		marginRight: theme.spacing(2),
		marginLeft: 0,
		width: '100%',
		[theme.breakpoints.up('sm')]: {
			marginLeft: theme.spacing(3),
			width: 'auto',
		},
	},
	searchIcon: {
		padding: theme.spacing(0, 2),
		height: '100%',
		position: 'absolute',
		pointerEvents: 'none',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	searchRoot: {
		color: 'inherit',
	},
	searchInput: {
		padding: theme.spacing(1, 1, 1, 0),
		// vertical padding + font size from searchIcon
		paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
		transition: theme.transitions.create('width'),
		width: '100%',
		[theme.breakpoints.up('md')]: {
			width: '40ch',
		},
	},
}))

/**
 * IGSearchBar component is the search bar of navbar.
 * This component dynamicaly update url parameter 'search' with the input value
 *
 * @returns JSX Elements
 */
export default function IGSearchBar() {
	const classes = useStyles()
	const history = useHistory()

	// States
	const [inputValue, setInputValue] = useState('')

	// Handle functions
	function handleInputChange(event) {
		event.preventDefault()
		history.replace(`?search=${event.target.value}`)
		setInputValue(event.target.value)
	}

	return (
		<div className={classes.search}>
			<div className={classes.searchIcon}>
				<SearchIcon />
			</div>
			<InputBase
				placeholder="Search …"
				value={inputValue}
				classes={{
					root: classes.searchRoot,
					input: classes.searchInput,
				}}
				inputProps={{ 'aria-label': 'search' }}
				onChange={handleInputChange}
			/>
		</div>
	)
}

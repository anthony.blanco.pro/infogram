// Infogram components
import DescriptionField from '@/components/Fields/DescriptionField/DescriptionField'

export default function PublicForm({
	type,
	description,
	data,
	onDescChange,
	onFileSelect,
	onTypeChange,
}) {
	return (
		<>
			<DescriptionField
				type={type}
				description={description}
				data={data}
				onDescChange={onDescChange}
				onFileSelect={onFileSelect}
				onTypeChange={onTypeChange}
			/>
		</>
	)
}
